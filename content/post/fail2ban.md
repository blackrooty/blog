+++
title = "Fail2Ban и море"
date = "2020-05-24"
tags = [
    "fail2ban",
    "cloudflare",
]
+++
<!--more-->
### AbuseIPDB
[Подключения модуля AbuseIPDB](https://docs.abuseipdb.com/?shell#introduction)
[Категории AbuseIPDB](https://www.abuseipdb.com/categories)

Источники для вдохновения:
- [Fail2ban HOWTOs](https://www.fail2ban.org/wiki/index.php/HOWTOs)
- [Fail2Ban tutorial](https://easyengine.io/tutorials/nginx/fail2ban/)
- [How To Protect an Nginx Server with Fail2Ban on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-protect-an-nginx-server-with-fail2ban-on-ubuntu-14-04)
- [How Fail2Ban Works to Protect Services on a Linux Server](https://www.digitalocean.com/community/tutorials/how-fail2ban-works-to-protect-services-on-a-linux-server)
- [Fail2Band cloudflare.conf  for site under attack](https://gist.github.com/crocodeio/c7195ce2566287ceefff024b97c97133)
- [Fail2ban and CloudFlare](https://www.aaflalo.me/2017/03/fail2ban-and-cloudflare/)
- [Unable to unban ip through fail2ban in cloudflare](https://community.cloudflare.com/t/unable-to-unban-ip-through-fail2ban-in-cloudflare/32343)
- [How to setup Cloudflare and fail2ban with automated “set_real_ip_from” in nginx](https://technicalramblings.com/blog/cloudflare-fail2ban-integration-with-automated-set_real_ip_from-in-nginx/)
- [How to Integrate fail2ban with CloudFlare API v4 Guide](https://guides.wp-bullet.com/integrate-fail2ban-cloudflare-api-v4-guide/)
- [Fail2Ban Custom Action](https://blog.redbranch.net/2013/02/28/fail2ban-custom-action/)
- [Fail2ban: an enemy of script-kiddies](https://debaday.debian.net/2007/04/29/fail2ban-an-enemy-of-script-kiddies/)
- [Using Unix Tools to Extract String Values/Extracting JSON values with Bash](https://stackoverflow.com/questions/1743105/using-unix-tools-to-extract-string-values)
- [Blokowanie ataku DoS przy użyciu nginx, fail2ban oraz Cloudflare.](https://flask.pl/blokowanie-ataku-dos-przy-uzyciu-nginx-fai2ban-cloudflare/)
- [Configuring IP Access Rules](https://support.cloudflare.com/hc/en-us/articles/217074967)
- [jq Tutorial](https://stedolan.github.io/jq/tutorial/)