---
title: "How to set up automatic updates on Ubuntu Server 18.04"
date: 2019-12-02T08:47:11+01:00
---
This guide explains how to configure automatic updates in Ubuntu Server 18.04 “Bionic Beaver”.
How to set up automatic updates on Ubuntu Server 18.04
This guide explains how to configure automatic updates in Ubuntu Server 18.04 “Bionic Beaver”.

Automatic Updates in Ubuntu Server
<!--more-->

### Step 1: package installation
```bash
Install the unattended-upgrades package:
sudo apt install unattended-upgrades
```
This package may already be installed on your server.

### Step 2: configure automatic updates
Edit the configuration file (here with nano – replace with any other text editor):
```bash
sudo nano /etc/apt/apt.conf.d/50unattended-upgrades
```
The beginning of the configuration file should look like this:
![install](/data/img/Install-unattended-upgrades-on-Ubuntu-Server.png)

Anything after a double slash “//” is a comment and has no effect. To “enable” a line, remove  the double slash at the beginning of the line (replace with nothing or with spaces to keep alignment).

The most important: uncomment the “updates” line by deleting the two slashes at the beginning of it:
```bash
"${distro_id}:${distro_codename}-updates";
```

Optional: You should uncomment and adapt the following lines to ensure you’ll be notified if an error happens:
```bash
Unattended-Upgrade::Mail "user@example.com";
Unattended-Upgrade::MailOnlyOnError "true";
```

Recommended: remove unused kernel packages and dependencies and make sure the system automatically reboots if needed by uncommenting and adapting the following lines:
```bash
Unattended-Upgrade::Remove-Unused-Kernel-Packages "true";
↑ You may have to add a semicolon at the end of this line. ↑
Unattended-Upgrade::Remove-Unused-Dependencies "true";
Unattended-Upgrade::Automatic-Reboot "true";
Unattended-Upgrade::Automatic-Reboot-Time "02:38";
```

### Step 3: enable automatic updates
Enable automatic updates and set up update intervals by running:
```bash
sudo nano /etc/apt/apt.conf.d/20auto-upgrades
```

In most cases, the file will be empty. Copy and paste the following lines:
```bash
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::AutocleanInterval "7";
APT::Periodic::Unattended-Upgrade "1";
```

The time interval is specified in days, feel free to change the values. 

### Step 4: check if it works
You can see if the auto-upgrades work by launching a dry run:
```bash
sudo unattended-upgrades --dry-run --debug
```

The dry run should output something like this:
![upgrade](Unattended-upgrades-on-Ubuntu-Server-18.04-Bionic-Beaver.png)
Another way to check if automatic updates work is waiting a few days and checking the unattended upgrades logs:
```bash
cat /var/log/unattended-upgrades/unattended-upgrades.log
```

Done! Ubuntu Server 18.04 should now update itself once a day.