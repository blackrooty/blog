---
title: "Complete uninstall Microsoft Visual Studio (Xamarin) including Mono and Microsoft NuGet trace"
date: 2019-12-01T08:47:11+01:00
---
<!--more-->
### Complete uninstall Microsoft Visual Studio (Xamarin) including Mono and Microsoft NuGet trace
```bash
#!/bin/bash
# Complete uninstall Microsoft Visual Studio (Xamarin) including Mono and Microsoft NuGet traces:

# Uninstall Xamarin Studio
rm -rf "/Applications/Xamarin Studio.app"
rm -rf "/Applications/Xamarin Profiler.app"
rm -rf "/Applications/Xamarin Workbooks.app"
rm -rf ~/Library/Caches/XamarinStudio-*
rm -rf ~/Library/Logs/XamarinStudio-*
rm -rf ~/Library/Preferences/XamarinStudio-*
rm -rf ~/Library/XamarinStudio-*
rm -rf ~/Library/Caches/Xamarin
rm -rf ~/Library/Caches/VisualStudio
rm -rf ~/Library/Developer/Xamarin
rm -rf ~/Library/Developer/XamarinStudio
rm -rf ~/Library/Developer/VisualStudio
rm -rf ~/Library/Logs/Xamarin
rm -rf ~/Library/Logs/VisualStudio
rm -rf ~/Library/Preferences/Xamarin
rm -rf ~/Library/Preferences/VisualStudio
rm -rf ~/Library/Xamarin
rm -rf ~/Library/MonoAndroid
rm -rf ~/Library/XamarinStudio-*
rm -rf ~/Library/VisualStudio
rm -rf ~/Library/Application\ Support/XamarinStudio-*
rm -rf ~/Library/Application\ Support/VisualStudio
rm -rf ~/.templateengine/Visual\ Studio
rm -rf ~/.share/Xamarin
rm -rf ~/.local/share/Xamarin

sudo rm -f /private/var/db/receipts/com.xamarin.*
sudo rm -f /private/var/db/receipts/xamarin.*

# Uninstall Mono MDK
# You will also want to make sure that there are no other dependencies on Mono
sudo rm -rf /Library/Frameworks/Mono.framework
sudo pkgutil --forget com.xamarin.mono-MDK.pkg
rm -rf ~/.local/share/MonoForAndroid
rm -rf ~/.config/.mono
rm -rf ~/.android

# Uninstall Xamarin.Android
sudo rm -rf /Developer/MonoDroid
sudo rm -rf /Library/Frameworks/Xamarin.Android.framework
sudo pkgutil --forget com.xamarin.android.pkg
rm -rf ~/Library/MonoAndroid

# Uninstall Xamarin.iOS
rm -rf ~/Library/MonoTouch
sudo rm -rf /Library/Frameworks/Xamarin.iOS.framework
sudo rm -rf /Developer/MonoTouch
sudo rm -rf /Developer/MonoAndroid
sudo pkgutil --forget com.xamarin.monotouch.pkg
sudo pkgutil --forget com.xamarin.xamarin-ios-build-host.pkg

## To Uninstall the Xamarin Build Host
sudo rm -rf "/Applications/Xamarin.iOS Build Host.app"

## To unload and remove the Xamarin Build Host launchd job
launchctl unload /Library/LaunchAgents/com.xamarin.mtvs.buildserver.plist
sudo rm -f /Library/LaunchAgents/com.xamarin.mtvs.buildserver.plist

# Uninstall Xamarin.Mac
sudo rm -rf /Library/Frameworks/Xamarin.Mac.framework
rm -rf ~/Library/Xamarin.Mac
sudo rm -rf /Library/Frameworks/Xamarin.Interactive.framework

# Uninstall Xamarin Installer
rm -rf ~/Library/Caches/XamarinInstaller/
rm -rf ~/Library/Logs/XamarinInstaller/
rm -rf ~/Library/Preferences/Xamarin/

# Uninstall Microsoft NuGet
rm -rf ~/.local/share/NuGet
rm -rf ~/.config/NuGet
rm -rf ~/.config/.NuGet
rm -rf ~/.NuGet

# Uninstall traces of visual studio code
rm -rf ~/.vscode
```