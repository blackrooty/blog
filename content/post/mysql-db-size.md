---
title: "Calculate MySQL DB size"
date: 2019-12-08T08:47:11+01:00
---
<!--more-->

```sql
SELECT table_schema AS "Database", 
ROUND(SUM(data_length + index_length) / 1024 / 1024, 2) AS "Size (MB)" 
FROM information_schema.TABLES 
GROUP BY table_schema
ORDER BY Size (MB) desc;
```
![mysql-db.size](/img/mysql-db.size.png)
